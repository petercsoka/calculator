<?php
abstract class AbstractOperation {

    protected $operation;

    public function __construct($operation)
    {
        $this->operation = $operation;
    }

    public function getOperation()
    {
        return $this->operation;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    abstract public function operate($value1, $value2);

    abstract public function isOperation();

}
?>