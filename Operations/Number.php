<?php

include_once __DIR__."/AbstractOperation.php";

class Number extends AbstractOperation {

    public $priority = 0;

    public function operate($value1, $value2) {
        return $this->operation;
    }

    public function isOperation(){
        return false;
    }
}
?>