<?php

include_once __DIR__."/AbstractOperation.php";

class Division extends AbstractOperation {

    public $priority = 2;

    public function operate($value1, $value2) {
        return $value1 / $value2;
    }

    public function isOperation(){
        return true;
    }

}
?>