<?php

include __DIR__."/OperationFactory.php";


class Calculator {

    public function calculate($string)
    {
        $elements = $this->split($string);
        $parsed = $this->parse_elements_to_operations($elements);
        $result = $this->do_calculation($parsed);

        return $result->getOperation();
    }

    public function parse_elements_to_operations($elements)
    {
        $operations = [];

        foreach($elements as $index => $element){

            $operation = OperationFactory::factory($element);
            $operations[] = $operation;

        }

        return $operations;

    }

    private function split($string)
    {
        $elements = preg_split('/(\d+|\+|-|\*|\/)|\s+/', $string, null, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        return $elements;
    }

    private function switch_operations($operation, $current_index, $op) {
        $val1 = $operation[$current_index-1]->getOperation();
        $val2 = $operation[$current_index+1]->getOperation();

        $operation[$current_index] = OperationFactory::factory($op->operate($val1,$val2));

        unset($operation[$current_index-1]);
        unset($operation[$current_index+1]);



        return array_values($operation);
    }

    private function do_calculation($operations)
    {
        $count = count($operations);
        do {

            foreach($operations as $index => $op) {

                if ($op->isOperation()){

                    if (isset($operations[$index+2]) && $operations[$index+2]->isOperation()) {

                        if ($op->priority > $operations[$index + 2]->priority) {

                            $operations = $this->switch_operations($operations, $index, $op);

                        } elseif ($op->priority == $operations[$index + 2]->priority) {

                            $operations = $this->switch_operations($operations, $index, $op);

                        }

                    } elseif (count($operations) == 3) {

                        $operations = $this->switch_operations($operations, 1, $op);

                    } elseif (count($operations) == $count-2) {

                        $operations = $this->switch_operations($operations, $index-2, $op);

                    }

                }

            }

        } while (count($operations) != 1 );

        return $operations[0];
    }

}

?>