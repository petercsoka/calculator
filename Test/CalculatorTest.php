<?php
include __DIR__."/../Calculator.php";

class CalculatorTest extends PHPUnit_Framework_TestCase {

    public function testCalculation(){

        $calculator = new Calculator();

        $result = $calculator->calculate("1 + 1 * 3 + 3");
        $this->assertEquals(7, $result);

        $result = $calculator->calculate("1 + 1 * 3 * 3");
        $this->assertEquals(10, $result);

        $result = $calculator->calculate("1 * 1 * 3 * 3");
        $this->assertEquals(9, $result);

    }

}
?>