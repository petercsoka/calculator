<?php
include __DIR__."/Operations/Addition.php";
include __DIR__."/Operations/Division.php";
include __DIR__."/Operations/Multiplication.php";
include __DIR__."/Operations/Substraction.php";
include __DIR__."/Operations/Number.php";

abstract class OperationFactory {

    public function factory($value){

        if(is_numeric($value)){
            return new Number($value);
        } elseif ($value == "+") {
            return new Addition($value);
        } elseif ($value == "-") {
            return new Substraction($value);
        } elseif ($value == "*") {
            return new Multiplication($value);
        } elseif ($value == "/") {
            return new Division($value);
        } else {
            die("No operation for: "+$value);
        }

    }

}
?>